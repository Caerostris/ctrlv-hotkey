/*
 * CtrlV-Hotkey - Pasting code has never been easier!
 *
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package main

import (
	"0x.cx/caerostris/ctrlv-go"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
)

func main() {
	// get the clipboard contents
	xsel := exec.Command("xsel")
	xselOut, err := xsel.StdoutPipe()
	if err != nil {
		fmt.Printf("Could not start xsel.\n")
	}

	xsel.Start()
	xselBytes, err := ioutil.ReadAll(xselOut)
	xsel.Wait()
	selection := string(xselBytes)

	if selection == "" {
		fmt.Printf("Nothing selected.\n")
		return
	}

	// upload and retrieve ctrlv-link
	res, err := ctrlv.NewPaste("Plain", "1 Month", selection)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	// overwrite the clipboard with the link
	fmt.Printf("%s\n", res)
	xsel = exec.Command("xsel", "-b", "-i")
	xselIn, err := xsel.StdinPipe()
	if err != nil {
		fmt.Printf("Could not start xsel\n")
	}

	xsel.Start()
	io.Copy(xselIn, bytes.NewBufferString(res))

	// show notification
	notify := exec.Command("notify-send", "CtrlV", "Link copied to clipboard! :)")
	notify.Start()
}
