#!/bin/bash

# Copyright (c) 2014 Keno Schwalb
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

GSETTINGS_PATH="org.gnome.settings-daemon.plugins.media-keys"
KEY_PATH="/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings"
GSETTINGS="gsettings set $GSETTINGS_PATH.custom-keybinding:$KEY_PATH"

i=0
NAME=$(gsettings get $GSETTINGS_PATH.custom-keybinding:$KEY_PATH/custom0/ name)
while [ "$NAME" != "''" ]
do
	if [ $NAME == "'CtrlV'" ]; then
		echo "Found. Not installing"
		exit
	fi
	i=$(( $i + 1 ))
	NAME=$(gsettings get $GSETTINGS_PATH.custom-keybinding:$KEY_PATH/custom$i/ name)
done

echo "Not found. Installing"

CK=$(gsettings get $GSETTINGS_PATH custom-keybindings)
if [ "$CK" == "@as []" ]; then
	gsettings set $GSETTINGS_PATH custom-keybindings "['$KEY_PATH/custom0/']"
else
	gsettings set $GSETTINGS_PATH custom-keybindings "${CK%]}, '$KEY_PATH/custom$i/']"
fi

$GSETTINGS/custom$i/ name "CtrlV"
$GSETTINGS/custom$i/ command "ctrlv-hotkey"
$GSETTINGS/custom$i/ binding "<Primary><Alt>c"
