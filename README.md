# CtrlV-Hotkey

This is a helper program for CtrlV.
Select some text, and just hit Ctrl-Alt-C to paste it to CtrlV!

## Installation

### Binary

Just download the latest [deb file](https://bitbucket.org/Caerostris/ctrlv-hotkey/downloads/ctrlv-hotkey_0.1.deb) and install it using your favourite package manager or

	$ sudo apt-get install xsel libnotify-bin
	$ sudo dpkg --install ctrlv-hotkey_0.1.deb

### Building yourself

	$ go get 0x.cx/caerostris/ctrlv-hotkey
	$ cd $GOPATH/src/0x.cx/caerostris/ctrlv-hotkey
	$ make build-dep
	$ sudo apt-get install xsel libnotify-bin
	$ sudo dpkg --install ctrlv-hotkey_0.1.deb

## License

All source files are released under the Mozilla Public License, v2.0
