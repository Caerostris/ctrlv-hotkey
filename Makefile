PACKAGE=ctrlv-hotkey
VERSION=0.1
BUILDDIR=/tmp/$(PACKAGE)_$(VERSION)

install:
	go install
	scripts/ctrlv_hotkey.sh

build-dep:
	go build
	mkdir -p $(BUILDDIR)/usr/bin
	mkdir -p $(BUILDDIR)/etc/xdg/autostart
	mkdir -p $(BUILDDIR)/usr/share/ctrlv/
	mkdir -p $(BUILDDIR)/DEBIAN
	cp ./$(PACKAGE) $(BUILDDIR)/usr/bin/
	cp scripts/ctrlv_hotkey.sh $(BUILDDIR)/usr/share/ctrlv/

	echo "[Desktop Entry]" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop
	echo "Type=Application" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop
	echo "Name=$(PACKAGE)" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop
	echo "Comment=Check keyboard shortcut for CtrlV" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop
	echo "Exec=/usr/share/ctrlv/ctrlv_hotkey.sh" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop
	echo "NoDisplay=true" >> $(BUILDDIR)/etc/xdg/autostart/$(PACKAGE).desktop

	echo "Package: $(PACKAGE)" >> $(BUILDDIR)/DEBIAN/control
	echo "Version: 1.0-1" >> $(BUILDDIR)/DEBIAN/control
	echo "Section: base" >> $(BUILDDIR)/DEBIAN/control
	echo "Priority: optional" >> $(BUILDDIR)/DEBIAN/control
	echo "Architecture: amd64" >> $(BUILDDIR)/DEBIAN/control
	echo "Depends: xsel (>= 1.1), libnotify-bin (>= 0.7)" >> $(BUILDDIR)/DEBIAN/control
	echo "Maintainer: Keno Schwalb <keno@kenoschwalb.com>" >> $(BUILDDIR)/DEBIAN/control
	echo "Description: CtrlV client for Ubuntu" >> $(BUILDDIR)/DEBIAN/control
	echo " Select your text and hit Ctrl-Alt-C." >> $(BUILDDIR)/DEBIAN/control
	echo " Bam - you've got a CtrlV-link to the selected text in your clipboard!" >> $(BUILDDIR)/DEBIAN/control
	dpkg-deb --build $(BUILDDIR) ./$(PACKAGE)_$(VERSION).deb
	rm -rf $(BUILDDIR)
	rm ./$(PACKAGE)
